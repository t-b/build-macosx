#!/bin/bash

set -x
set -e

export TANGO_BASE=${HOME}/soft/installed/tango
export OMNI_BASE=/usr/local/opt/omniorb
export ZMQ_BASE=/usr/local/opt/zeromq

# brew install omniorb zeromq

mkdir tango || true
cd tango

curr=$(pwd)

git clone https://github.com/zeromq/cppzmq.git || true
cd cppzmq
git fetch
git clean -fdx
git co v4.7.1

cmake -B build -DCMAKE_INSTALL_PREFIX=${TANGO_BASE} -DCPPZMQ_BUILD_TESTS=OFF
make -C build -j12 && make -C build install
cd $curr

git clone https://gitlab.com/tango-controls/tango-idl.git || true
cd tango-idl
git fetch
git clean -fdx
cmake -B build -DCMAKE_INSTALL_PREFIX=${TANGO_BASE}
make -C build -j12 && make -C build install
cd $curr

git clone https://gitlab.com/tango-controls/cppTango.git || true
cd cppTango
git fetch
git clean -fdx
git checkout main
git reset --hard @{u}
cmake -B build . -DCMAKE_INSTALL_PREFIX=${TANGO_BASE} -DTANGO_IDL_BASE=${TANGO_BASE} -DTANGO_ZMQ_BASE=/opt/homebrew -DTANGO_OMNI_BASE=/opt/homebrew -DTANGO_CPPZMQ_BASE=${TANGO_BASE} -DBUILD_TESTING=ON -DTANGO_USE_PCH=OFF -DCMAKE_VERBOSE_MAKEFILE=TRUE -DOMNIORB_VERSION=4.2.5 -DTANGO_OMNIIDL_PATH=/opt/homebrew/bin -DTANGO_USE_JPEG=OFF

make -C build -j12 > compile.log
make -C build install
cd $curr

git clone https://gitlab.com/tango-controls/tango_admin.git || true
cd tango_admin
git fetch
git clean -fdx
git checkout main
git reset --hard @{u}
cmake -B build . -DTango_ROOT=${TANGO_BASE} -DCMAKE_INSTALL_PREFIX=${TANGO_BASE}
make -C build -j12
make -C build install
cd $curr

git clone https://gitlab.com/tango-controls/TangoTest.git || true
cd TangoTest
git fetch
git clean -fdx
git checkout main
git reset --hard @{u}
cmake -B build . -DTango_ROOT=${TANGO_BASE} -DCMAKE_INSTALL_PREFIX=${TANGO_BASE}
make -C build -j12
make -C build install
cd $curr
